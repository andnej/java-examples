package id.co.knt.airport.web;

import id.co.knt.airport.entity.Address;
import id.co.knt.airport.entity.Passenger;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named("passengerBean")
@Stateful
@SessionScoped
public class PassengerBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2144377619157882801L;
	
	private static Logger log = LoggerFactory.getLogger(PassengerBean.class);

	@PersistenceContext(type=PersistenceContextType.EXTENDED)
	private EntityManager em;
	private Passenger selectedPassenger;

	
	@PostConstruct
	public void init(){
		reset();
	}
	
	public void reset(){
		selectedPassenger = new Passenger();
	}
	
	public String create() {
		reset();
		
		return null;
	}

	public String delete(){
		em.remove(selectedPassenger);

		return null;
	}

	public String persist() {
		em.merge(selectedPassenger);
		FacesContext context =  FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Saved!"));
		RequestContext request = RequestContext.getCurrentInstance();
		request.execute("passengerDialog.hide()");
		log.info("RESET CALLED by persist");

		reset();
		
		return null;
	}
	
	public void onClose(CloseEvent e) {
	}

	public Passenger getSelectedPassenger() {
		return selectedPassenger;
	}
	
	public void setSelectedPassenger(Passenger selectedPassenger) {
		this.selectedPassenger = em.merge(selectedPassenger);
		if (this.selectedPassenger.getAddress() == null) {
			this.selectedPassenger.setAddress(new Address());
		}
	}
}

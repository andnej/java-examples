package id.co.knt.airport.web;

import id.co.knt.airport.dto.PassengerFilter;
import id.co.knt.airport.entity.Airport;
import id.co.knt.airport.entity.FareType;
import id.co.knt.airport.entity.FlightSchedule;
import id.co.knt.airport.entity.Passenger;
import id.co.knt.airport.entity.Payment;
import id.co.knt.airport.entity.TicketClass;
import id.co.knt.airport.qualifier.Arrival;
import id.co.knt.airport.qualifier.Departure;
import id.co.knt.airport.qualifier.Filtered;
import id.co.knt.airport.qualifier.FromDate;
import id.co.knt.airport.qualifier.ToDate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class DefaultFactoryBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5975921542119247751L;

	@PersistenceContext
	private EntityManager em;

	@Produces
	@Named
	public List<Airport> getAirports() {
		return em.createNamedQuery("Airport.findAll", Airport.class)
				.getResultList();
	}

	@Produces
	@Named
	public List<Passenger> getPassengers() {
		return em.createNamedQuery("Passenger.findAll", Passenger.class)
				.getResultList();
	}

	@Produces
	@Filtered
	@Named("filteredPassengers")
	public List<Passenger> getFilteredPassengers(PassengerFilter passengerFilter) {
		List<Passenger> passengers = getPassengers();
		List<Passenger> filteredPassengers = new ArrayList<Passenger>();
		if (passengerFilter != null) {
			for (Passenger passenger : passengers) {
				if (passengerFilter.isQualify(passenger)) {
					passengers.add(passenger);
				}
			}
		} else {
			filteredPassengers = passengers;
		}

		return filteredPassengers;
	}

	@Produces
	@Named
	public List<TicketClass> getTicketClasses() {
		return em.createNamedQuery("TicketClass.findAll", TicketClass.class)
				.getResultList();
	}

	@Produces
	@Named
	public List<FareType> getFareTypes() {
		return em.createNamedQuery("FareType.findAll", FareType.class)
				.getResultList();
	}

	@Produces
	@Named
	public List<FlightSchedule> getFlightSchedules() {
		return em.createNamedQuery("FlightSchedule.findAll",
				FlightSchedule.class).getResultList();
	}

	@Produces
	@Named
	public List<FlightSchedule> getFilteredFlightSchedules(
			@Departure Airport departure, @Arrival Airport arrival) {
		List<FlightSchedule> flightSchedules = getFlightSchedules();
		List<FlightSchedule> candidates = new ArrayList<FlightSchedule>(
				flightSchedules);
		if (arrival != null) {
			for (FlightSchedule flightSchedule : flightSchedules) {
				if (!flightSchedule.getArrivalAirport().equals(arrival)) {
					candidates.remove(flightSchedule);
				}
			}
		}
		if (departure != null) {
			for (FlightSchedule flightSchedule : flightSchedules) {
				if (!flightSchedule.getDepartureAirport().equals(departure)) {
					candidates.remove(flightSchedule);
				}
			}
		}
		return candidates;
	}

	@Produces
	@Named
	public List<FlightSchedule> getFilteredFlightSchedulesByDate(
			@FromDate Date fromDate, @ToDate Date toDate) {
		List<FlightSchedule> flightSchedules = getFlightSchedules();
		List<FlightSchedule> filters = new ArrayList<FlightSchedule>(
				flightSchedules);
		if (fromDate != null) {
			for (FlightSchedule flightSchedule : flightSchedules) {
				if (flightSchedule.getDepartureDate().before(fromDate)) {
					filters.remove(flightSchedule);
				}
			}
		}
		if (toDate != null) {
			for (FlightSchedule flightSchedule : flightSchedules) {
				if (flightSchedule.getDepartureDate().after(toDate)) {
					filters.remove(flightSchedule);
				}
			}
		}
		return filters;
	}
	
	public List<Payment> getPayments() {
		return em.createNamedQuery("Payment.findAll", Payment.class)
				.getResultList();
	}

}

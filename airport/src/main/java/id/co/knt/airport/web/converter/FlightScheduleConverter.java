package id.co.knt.airport.web.converter;

import id.co.knt.airport.entity.FlightSchedule;

import javax.ejb.Stateless;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Named("flightScheduleConverter")
@Stateless
public class FlightScheduleConverter implements Converter{
	
	@PersistenceContext
	private EntityManager em;
	
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		if (arg2 == null || arg2.equals(""))
			return null;
	    Long id = Long.valueOf(arg2);
	    FlightSchedule flightSchedule = em.find(FlightSchedule.class, id);
	    return flightSchedule;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		if (!(arg2 instanceof FlightSchedule))
			return "";
	    FlightSchedule flightSchedule = (FlightSchedule) arg2;
	    Long id = flightSchedule.getId();
	    return String.valueOf(id);
	}

	
}

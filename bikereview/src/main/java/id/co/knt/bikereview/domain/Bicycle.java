package id.co.knt.bikereview.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="BICYCLES")
@NamedQueries({
	@NamedQuery(name="Bicycle.findAll", query="select b from Bicycle b")
})
public class Bicycle implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7046328863315694291L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID")
	private Long id;
	@Column(name="FACTORY")
	private String factory;
	@Column(name="NAME")
	private String name;
	@Column(name="BUILD_YEAR")
	private String year;
	
	public Bicycle(String factory, String name, String year) {
		super();
		this.factory = factory;
		this.name = name;
		this.year = year;
	}
	
	public Bicycle() {
		super();
	}
	
	public String getFactory() {
		return factory;
	}
	
	public void setFactory(String factory) {
		this.factory = factory;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getYear() {
		return year;
	}
	
	public void setYear(String year) {
		this.year = year;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Bicycle [factory=" + factory + ", name=" + name + ", year="
				+ year + "]";
	}
}

package id.co.knt.bikereview.service;

import id.co.knt.bikereview.domain.Bicycle;

import java.io.Serializable;
import java.util.List;

public interface BicycleService extends Serializable {

	public abstract List<Bicycle> findAll();

	public abstract void save(Bicycle b);

	public abstract void delete(Bicycle b);

}
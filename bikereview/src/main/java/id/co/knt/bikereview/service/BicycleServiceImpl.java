package id.co.knt.bikereview.service;

import id.co.knt.bikereview.domain.Bicycle;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Named("bicycleService")
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class BicycleServiceImpl implements BicycleService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2768450458368195089L;
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Bicycle> findAll() {
		return em.createNamedQuery("Bicycle.findAll", Bicycle.class)
				.getResultList();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void save(Bicycle b) {
		em.merge(b);
		System.out.println("saved called and ended, bike:"+b.toString());
	}

	@Override
	public void delete(Bicycle b) {
		Bicycle bt = em.find(Bicycle.class, b.getId());
		em.remove(bt);
	}

}

package id.web.knt.po.web;

import id.web.knt.po.entity.Product;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Event;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.primefaces.context.RequestContext;

@Named("productBean")
@Stateful
@SessionScoped
public class ProductBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@PersistenceContext(type=PersistenceContextType.EXTENDED)
	private EntityManager em;
	@Inject
	private Event<Product> productEvent;

	private List<Product> products;
	private Product selectedProduct;
	
	
	private boolean dataVisible = false;
	
	@PostConstruct
	public void init() {
		System.out.println("init called");
		reset();
	}
	
	public String findAll() {
		products = em.createNamedQuery("Product.findAll", Product.class)
				.getResultList();
		dataVisible = !products.isEmpty();
		
		return null;
	}
	
	public void reset() {
		findAll();
		selectedProduct = new Product();
		System.out.println("RESET CALLED by Product");
	}
	
	public String create() {
		reset();
		return null;
	}
	
	public String delete() {
		em.remove(selectedProduct);
		findAll();
		reset();
		
		return null;
	}
	
	public String persist() {
		productEvent.fire(em.merge(selectedProduct));
		FacesContext context =  FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Saved!"));
		RequestContext request = RequestContext.getCurrentInstance();
		request.execute("productDialog.hide()");
		
		reset();
		
		return null;
	}

	public Product getSelectedProduct() {
		return selectedProduct;
	}


	public void setSelectedProduct(Product selectedProduct) {
		this.selectedProduct = selectedProduct;
	}


	public boolean isDataVisible() {
		return dataVisible;
	}


	public void setDataVisible(boolean dataVisible) {
		this.dataVisible = dataVisible;
	}



	public List<Product> getProducts() {
		return products;
	}
	
}

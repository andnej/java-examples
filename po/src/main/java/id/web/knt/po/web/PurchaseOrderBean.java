package id.web.knt.po.web;

import id.web.knt.po.entity.Employee;
import id.web.knt.po.entity.Product;
import id.web.knt.po.entity.PurchaseOrder;
import id.web.knt.po.entity.PurchaseOrderDetail;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.primefaces.event.CloseEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named("purchaseOrderBean")
@Stateful
@SessionScoped
public class PurchaseOrderBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2018629357786988296L;
	private static final Logger log = LoggerFactory.getLogger(PurchaseOrderBean.class);

	@PersistenceContext(type=PersistenceContextType.TRANSACTION)
	private EntityManager em;
	
	private List<PurchaseOrder> purchaseOrders = new ArrayList<PurchaseOrder>();
	private List<Employee> users = new ArrayList<Employee>();
	private List<Product> products = new ArrayList<Product>();
	private PurchaseOrder selectedPurchaseOrder;
	private PurchaseOrderDetail selectedPurchaseOrderDetail;
	private boolean dataVisible = false;
	
	@PostConstruct
	public void init() {
		users = em.createNamedQuery("Employee.findAll",Employee.class).getResultList();
		products = em.createNamedQuery("Product.findAll", Product.class).getResultList();
		findAll();
		reset();
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String findAll() {
		purchaseOrders.clear();
		purchaseOrders.addAll(em.createNamedQuery("PurchaseOrder.findAll", PurchaseOrder.class)
				.getResultList());
		dataVisible = !purchaseOrders.isEmpty();
		return null;
	}
	
	public void reset() {
		selectedPurchaseOrder = new PurchaseOrder();
		selectedPurchaseOrderDetail = new PurchaseOrderDetail();
	}
	
	public String delete() {
		em.remove(selectedPurchaseOrder);
		
		return findAll();
	}
	
	public void onProductUpdate(@Observes Product updated) {
		boolean update = false;
		for (Product product : products) {
			if (product.equals(updated)) {
				update = true;
				product.setName(updated.getName());
				product.setPrice(updated.getPrice());
				break;
			}
		}
		if (!update) {
			products.add(updated);
		}
	}
	
	public String addPurchaseOrderDetail() {
		boolean found = false;
		for (PurchaseOrderDetail purchaseOrderDetail : selectedPurchaseOrder.getDetails()) {
			if (purchaseOrderDetail.getProduct().equals(selectedPurchaseOrderDetail.getProduct())) {
				purchaseOrderDetail.setQuantity(purchaseOrderDetail.getQuantity()+selectedPurchaseOrderDetail.getQuantity());
				found=true;
				break;
			}
		}
		if(found==false){
			selectedPurchaseOrder.addDetail(selectedPurchaseOrderDetail);
		}
		selectedPurchaseOrder.calculateTotal();
		System.out.println("Total : "+selectedPurchaseOrder.getTotal()); 
		selectedPurchaseOrderDetail = new PurchaseOrderDetail();
		return null;
	}
	
	public String persist() {
		log.info("User is not null ? " + selectedPurchaseOrder.getUser() != null ? "true" : "false");
		em.merge(selectedPurchaseOrder);
		
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Saved!"));
		
		return findAll();
	}
	
	public void onClose(CloseEvent e) {
		log.info("onClose Called");
		reset();
//		findAll();
	}
	
	public String refreshEntity(){
//		throw new CloseException();
		List<PurchaseOrderDetail> trash = new ArrayList<PurchaseOrderDetail>();
		for (PurchaseOrderDetail purchaseOrderDetail : selectedPurchaseOrder.getDetails()) {
			if (purchaseOrderDetail.getId() == null) {
				trash.add(purchaseOrderDetail);
			}
		}
		for (PurchaseOrderDetail toDelete : trash) {
			selectedPurchaseOrder.removeDetail(toDelete);
		}
		em.refresh(selectedPurchaseOrder);
		return findAll();
	}

	public PurchaseOrder getSelectedPurchaseOrder() {
		return selectedPurchaseOrder;
	}

	public void setSelectedPurchaseOrder(PurchaseOrder selectedPurchaseOrder) {
		this.selectedPurchaseOrder = em.find(PurchaseOrder.class, selectedPurchaseOrder.getId());
		selectedPurchaseOrder.calculateTotal();
		selectedPurchaseOrderDetail = new PurchaseOrderDetail();
	}

	public List<PurchaseOrder> getPurchaseOrders() {
		return purchaseOrders;
	}

	public List<Product> getProducts() {
		return products;
	}

	public boolean isDataVisible() {
		return dataVisible;
	}

	public void setDataVisible(boolean dataVisible) {
		this.dataVisible = dataVisible;
	}

	public List<Employee> getUsers() {
		return users;
	}

	public void setUsers(List<Employee> users) {
		this.users = users;
	}

	public PurchaseOrderDetail getSelectedPurchaseOrderDetail() {
		return selectedPurchaseOrderDetail;
	}

	public void setSelectedPurchaseOrderDetail(
			PurchaseOrderDetail selectedPurchaseOrderDetail) {
		this.selectedPurchaseOrderDetail = selectedPurchaseOrderDetail;
	}
	
	
}

package id.web.knt.po.web.converter;

import id.web.knt.po.entity.Employee;

import javax.ejb.Stateless;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named("employeeConverter")
@Stateless
public class EmployeeConverter implements Converter{
	@PersistenceContext
	EntityManager em;
	
	private static final Logger log = LoggerFactory.getLogger(EmployeeConverter.class);

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		log.info("getAsObject arg2 : "+ arg2);
		if (arg2 == null || arg2.equals(""))
			return null;
	    Long id = Long.valueOf(arg2);
	    Employee employee = em.find(Employee.class, id);
	    return employee;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		log.info("getAsString arg2 : "+ arg2);
		if (!(arg2 instanceof Employee))
			return "";
	    Employee employee = (Employee) arg2;
	    Long id = employee.getId();
	    return String.valueOf(id);
	}

}

package id.web.knt.po.web;

import id.web.knt.po.entity.Address;
import id.web.knt.po.entity.Employee;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Named("employeeBean")
@Stateful
@SessionScoped
public class EmployeeBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger("service");
	
	@PersistenceContext(type=PersistenceContextType.EXTENDED)
	private EntityManager em;
	private List<Employee> employes;
	private Employee selectedEmployee;
	private boolean dataVisible = false;
	
	@PostConstruct
	public void init(){
		findAll();
		log.info("RESET CALLED by init");
		reset();
	}
	
	public String findAll(){
		employes = em.createNamedQuery("Employee.findAll",Employee.class).getResultList();
		log.info("Employee : "+employes.size());
		dataVisible = !employes.isEmpty();
		return null;
	}
	
	private void log() {
		if (selectedEmployee.getAddress() != null)
			log.info(selectedEmployee.getAddress().toString());
		else
			log.info("address is null");
	}
	
	public void reset(){
		selectedEmployee = new Employee();
		log();
		log.info("RESET CALLED");
	}
	
//	public String create(){
//		reset();
//		return "employee";
//	}
	
	public String delete(){
		em.remove(selectedEmployee);
		log.info("RESET CALLED by delete");

		reset();
		return findAll();
	}
	
	public String persist() {
		log.info("First Name : "+selectedEmployee.getFirstName());
		log.info("Last Name : "+selectedEmployee.getLastName());
		log();
		em.merge(selectedEmployee);
		FacesContext context =  FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Saved!"));
		RequestContext request = RequestContext.getCurrentInstance();
		request.execute("employeeDialog.hide()");
		log.info("RESET CALLED by persist");

		reset();
		
		return findAll();
	}
	
	public void onClose(CloseEvent e) {
		reset();
	}

	public Employee getSelectedEmployee() {
		return selectedEmployee;
	}

	public void setSelectedEmployee(Employee selectedEmployee) {
		this.selectedEmployee = selectedEmployee;
		if (selectedEmployee != null) {
			log.info("First Name : "+selectedEmployee.getFirstName());
			log.info("Last Name : "+selectedEmployee.getLastName());
			log();
			if (selectedEmployee.getAddress() == null) {
				selectedEmployee.setAddress(new Address());
			}
		}
	}

	public boolean isDataVisible() {
		return dataVisible;
	}

	public void setDataVisible(boolean dataVisible) {
		this.dataVisible = dataVisible;
	}

	public List<Employee> getEmployes() {
		return employes;
	}


	
}
